# 整合到easy-admin

## Context-Path问题

- 不但要修改后端的context-path配置，还要修改前端的admin/config/pear.config.yml
- login.html中的/api/v1/login前面，但看起来不用改。
- OpenBrowserHandler里面也要修改。但这个只是个辅助工具，方便点击运行按钮时候自动启动浏览器。
- oss的路径也需要修改
  
  ## hutool版本问题
- 使用最新版也会出现一些问题，对easy-admin代码进行了修改，能运行了。

## 认证问题

如果全都放在localhost运行，easy-admin和jimureport都正常。但如果部署在服务器上，在客户端访问，即便easy-admin正常登录了，但把jimureport配置到菜单中点击的时候，sa-token却不能取到合法的session cookie值，导致认证通不过，无法访问jimureport。这可能跟easy-admin打开url的方式有关系。

现在的解决办法是把jimureport的url都放行，到jimureport自己的token校验拦截器中校验。但是这会导致能看报表的就能设计报表。所以还需要使用不同的token区分角色。此功能现在还没做。

等有了中移动扫描结果，准备将jimureport集成到别的框架中看看。

或者如果非要用easy-admin，可以到时候再详细研究原因。现在不研究了。

## 生成token

- 运行JimuReportTokenService的genToken 方法生成token。目前是一个固定的token，将来再考虑变成从数据表中获取。

## 账号

admin/lakernote

# jimureport-example

积木报表集成示例代码。

```
采用mysql5.7数据库
```

使用步骤
-----------------------------------

- 第一步：执行初始化脚步（自动创建数据库jimureport）
  
          db/jimureport.mysql5.7.create.sql
- 第三步： 启动项目（右键运行）
  
          org.jeecg.modules.JimuReportApplication
- 第四步： 访问项目
  
          http://localhost:8085/jmreport/list

Docker镜像制作
-----------------------------------

- 第一步：下载项目
  
        git clone https://gitee.com/jeecg/JimuReport.git
- 第二步：进入项目 jimureport-example 根目录
  
        cd JimuReport/jimureport-example
- 第三步：maven执行package
  
        mvn clean package
- 第四步：执行命令，生成镜像
  
        docker-compose up -d
- 第五步：访问报表
  
       http://localhost:8085/jmreport/list