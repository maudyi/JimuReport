package com.laker.admin.framework.utils;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.joda.time.Days;

import java.lang.management.ManagementFactory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

import org.apache.commons.lang3.time.DateFormatUtils;

/**
 * 时间工具类
 * 
 * @author ruoyi
 */
public class DateUtils extends org.apache.commons.lang3.time.DateUtils
{
    /**
	  * @Fields DAYS_B4_19700101 : 1970年之前到公元0年包含了多少天 
	  */
	private static final int DAYS_B4_19700101 = 719528;

	public static String YYYY = "yyyy";

    public static String YYYY_MM = "yyyy-MM";

    public static String YYYY_MM_DD = "yyyy-MM-dd";

    public static String YYYYMMDD = "yyyyMMdd";

    public static String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";

    public static String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

    public static String YYYY_MM_DD_HH_MM_SS_SSS = "yyyy-MM-dd HH:mm:ss.SSS";

    private static String[] parsePatterns = {
            "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM", "yyyyMM",
            "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy/MM",
            "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm", "yyyy.MM"};

    /**
     * 获取当前Date型日期
     * 
     * @return Date() 当前日期
     */
    public static Date getNowDate()
    {
        return new Date();
    }

    /**
     * 获取当前日期字符串, 默认格式为yyyy-MM-dd
     * 
     * @return String
     */
    public static String getDate()
    {
        return dateTimeNow(YYYY_MM_DD);
    }

    public static final String getTime()
    {
        return dateTimeNow(YYYY_MM_DD_HH_MM_SS);
    }

    public static final String dateTimeNow()
    {
        return dateTimeNow(YYYYMMDDHHMMSS);
    }

    public static final String dateTimeNow(final String format)
    {
        return parseDateToStr(format, new Date());
    }


    /**
     * 格式化为yyyy-MM-dd的字符串
     */
    public static final String dateTime(final Date date)
    {
        return parseDateToStr(YYYY_MM_DD, date);
    }

    /**
     * 格式化为yyyy-MM-dd HH:mm:ss的字符串
     */
    public static final String dateTimeFull(final Date date)
    {
        return parseDateToStr(YYYY_MM_DD_HH_MM_SS, date);
    }

    /**
     * 格式化给定的Date为字符串，按指定的pattern。
     */
    public static final String parseDateToStr(final String format, final Date date)
    {
        return new SimpleDateFormat(format).format(date);
    }
    
    /**
     * 解析字符串，返回Date对象，按给定pattern
     */
    public static final Date dateTime(final String format, final String ts)
    {
        try
        {
            return new SimpleDateFormat(format).parse(ts);
        }
        catch (ParseException e)
        {
            throw new RuntimeException(e);
        }
    }

    /**
     * 日期路径 即年/月/日 如2018/08/08
     */
    public static final String datePath()
    {
        Date now = new Date();
        return DateFormatUtils.format(now, "yyyy/MM/dd");
    }

    /**
     * 日期路径 即年-月-日 如2018-08-08
     */
    public static final String dateTime()
    {
        Date now = new Date();
		// maudyi modified: yyyyMMdd很不常用。
        return DateFormatUtils.format(now, "yyyy-MM-dd");
    }

    /**
     * 日期型字符串转化为日期格式，自动匹配各种 patterns
     */
    public static Date parseDate(Object str)
    {
        if (str == null)
        {
            return null;
        }
        try
        {
            return parseDate(str.toString(), parsePatterns);
        }
        catch (ParseException e)
        {
            return null;
        }
    }

    /**
     * 获取服务器启动时间
     */
    public static Date getServerStartDate()
    {
        long time = ManagementFactory.getRuntimeMXBean().getStartTime();
        return new Date(time);
    }

    /**
     * 计算相差天数
     */
    public static int differentDaysByMillisecond(Date date1, Date date2)
    {
        return Math.abs((int) ((date2.getTime() - date1.getTime()) / (1000 * 3600 * 24)));
    }

    /**
     * 计算两个时间差
     */
    public static String getDatePoor(Date endDate, Date nowDate)
    {
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - nowDate.getTime();
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;
        return day + "天" + hour + "小时" + min + "分钟";
    }
	
	
    /**
     * 增加 LocalDateTime ==> Date
     */
    public static Date toDate(LocalDateTime temporalAccessor)
    {
        ZonedDateTime zdt = temporalAccessor.atZone(ZoneId.systemDefault());
        return Date.from(zdt.toInstant());
    }

    /**
     * 增加 LocalDate ==> Date
     */
    public static Date toDate(LocalDate temporalAccessor)
    {
        LocalDateTime localDateTime = LocalDateTime.of(temporalAccessor, LocalTime.of(0, 0, 0));
        ZonedDateTime zdt = localDateTime.atZone(ZoneId.systemDefault());
        return Date.from(zdt.toInstant());
    }
    
	/*********************** 以下均为maudyi added ***********************************/
	
	public static final String getTimestamp(){
        return parseDateToStr(YYYY_MM_DD_HH_MM_SS_SSS, new Date());
    }
	
    /**
     * 模拟mysql的to_days()函数
     */
    public static int toDays(Date date) {
    	return DAYS_B4_19700101 + Days.daysBetween(new org.joda.time.LocalDate(0), new org.joda.time.LocalDate(date.getTime())).getDays();
    }

    /**
     * 模拟mysql的to_days()函数
     */
    public static int toDays(String dateStr) {
        Date date = parseDate(dateStr);
        return DAYS_B4_19700101 + Days.daysBetween(new org.joda.time.LocalDate(0), new org.joda.time.LocalDate(date.getTime())).getDays();
    }
    
    /**
     * 反写解析mysql的to_days()函数，返回日期对象
     */
    public static Date parseDaysToDate(int days) {
    	int offset = days - DAYS_B4_19700101;
    	Calendar calendar = Calendar.getInstance();
    	calendar.set(1970, 0, 1);
        calendar.add(Calendar.DAY_OF_YEAR, offset);
        // 必须设置到毫秒，否则返回的不是0点的date对象。
        return cutHHMMssSSS(calendar.getTime());
    }
    
    /**
     * 反写解析mysql的to_days()函数，返回日期字符串
     */
    public static String parseDaysToString(int days) {
    	int offset = days - DAYS_B4_19700101;
    	Calendar calendar = Calendar.getInstance();
    	calendar.set(1970, 0, 1);
    	calendar.add(Calendar.DAY_OF_YEAR, offset);
    	return dateTime(calendar.getTime());
    }

    /**
     * 把一个yyyy-MM-dd格式的字符串时间进行滚动
     * @param startDateStr: 起始日期字符串
     * @param delta : 滚动的天数
     */
    public static String addDays(String startDateStr, int delta){
        Date startDate = DateUtils.parseDate(startDateStr);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        calendar.add(Calendar.DAY_OF_YEAR, delta);
        Date endDate = calendar.getTime();
        String endDateStr = DateUtils.dateTime(endDate);
        return endDateStr;
    }

    /**
     * 把一个yyyy-MM-dd格式的字符串时间进行滚动
     * @param startDate: 起始日期字符串
     * @param delta : 滚动的天数
     */
    public static Date addDays(Date startDate, int delta){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        calendar.add(Calendar.DAY_OF_YEAR, delta);
        return calendar.getTime();
    }

    /**
     * 把一个日期往前往后推多少天。
     * @param date
     * @param field: Calendar.DAY_OF_MONTH
     * @param delta
     * @return
     */
    public static Date calendarAdd(Date date, int field, int delta){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(field, delta);
        return calendar.getTime();
    }

    /**
     * 把一个日期往前往后推多少天。
     * @param dateStr
     * @param field
     * @param delta
     * @return
     */
    public static Date calendarAdd(String dateStr, int field, int delta){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(parseDate(dateStr));
        calendar.add(field, delta);
        return calendar.getTime();
    }

    /**
     * 解析类似March, 2019这种格式的日期字符串
     * @param toCrawlDateStr
     * @return
     */
    public static Optional<Date> parseDateByLocale(String toCrawlDateStr, String pattern, Locale locale) {
        try {
            SimpleDateFormat sf = new SimpleDateFormat(pattern, locale);
            return Optional.ofNullable(sf.parse(toCrawlDateStr));
        } catch (ParseException e) {
            return Optional.empty();
        }
    }

    /**
     * 获取参数date所代表的时间所属月份的第一天
     *
     * @return
     */
    public static Date getFirstDayOfMonth(Date theDate) {
        Calendar calendar = Calendar.getInstance();
        GregorianCalendar gcFirstDay = (GregorianCalendar) Calendar.getInstance();
        gcFirstDay.setTime(theDate);
        gcFirstDay.set(Calendar.DAY_OF_MONTH, 1);
        gcFirstDay.set(Calendar.HOUR_OF_DAY, 0);
        gcFirstDay.set(Calendar.MINUTE, 0);
        gcFirstDay.set(Calendar.SECOND, 0);
        return gcFirstDay.getTime();
    }

    /**
     * 获取参数date所代表的时间所属月份的最后一天
     *
     * @return
     */
    public static Date getLastDayOfMonth(Date theDate) {
        Calendar calendar = Calendar.getInstance();
        GregorianCalendar gcLastDay = (GregorianCalendar) Calendar.getInstance();
        gcLastDay.setTime(getFirstDayOfMonth(theDate));
        gcLastDay.add(Calendar.MONTH, 1);
        gcLastDay.add(Calendar.DAY_OF_MONTH, -1);
        gcLastDay.add(Calendar.HOUR_OF_DAY, 23);
        gcLastDay.add(Calendar.MINUTE, 59);
        gcLastDay.add(Calendar.SECOND, 59);
        return gcLastDay.getTime();
    }

    /**
     * 把一个date对象的时分秒都截断，变成 00:00:00 000
     * @param originalDate
     * @return
     */
    public static Date cutHHMMssSSS(Date originalDate){
        return parseDate(dateTime(originalDate));
    }

    /**
     * 生成对日期的过滤条件的Date对象，或者是 00:00:00.000，或者是23:59:59.999
     * @param originalDate
     * @param beginTrueOrEndFalse
     * @return
     */
    public static Date reformatCriteriaDateTime(Date originalDate, boolean beginTrueOrEndFalse){
        if(beginTrueOrEndFalse){
            // yyyy-MM-dd 00:00:00
            String startDateStr = dateTime(originalDate);
            startDateStr += " 00:00:00";
            return dateTime(YYYY_MM_DD_HH_MM_SS, startDateStr);
        } else {
            // yyyy-MM-dd 23:59:59
            String endDateStr = dateTime(originalDate);
            endDateStr += " 23:59:59";
            // 发现sqlserver进行时间比对的时候，如果带上毫秒，就会出错。所以现在改为仅到秒。
            return dateTime(YYYY_MM_DD_HH_MM_SS, endDateStr);
        }
    }

    /**
     * Date转LocalDate
     * @param date
     */
    public static LocalDate date2LocalDate(Date date) {
        if(null == date) {
            return null;
        }
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    /**
     * LocalDate转Date
     * @param localDate
     * @return
     */
    public static Date localDate2Date(LocalDate localDate) {
        if(null == localDate) {
            return null;
        }
        ZonedDateTime zonedDateTime = localDate.atStartOfDay(ZoneId.systemDefault());
        return Date.from(zonedDateTime.toInstant());
    }


    public static void main(String[] args) {
        System.out.println(reformatCriteriaDateTime(new Date(), false));

	}
}
