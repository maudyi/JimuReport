package com.laker.admin.framework.utils;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Date;

/**
 * 字符串格式化
 * 
 * @author ruoyi
 */
public class StrFormatter
{
    public static final String EMPTY_JSON = "{}";
    public static final char C_BACKSLASH = '\\';
    public static final char C_DELIM_START = '{';
    public static final char C_DELIM_END = '}';

    /**
     * 格式化字符串<br>
     * 此方法只是简单将占位符 {} 按照顺序替换为参数<br>
     * 如果想输出 {} 使用 \\转义 { 即可，如果想输出 {} 之前的 \ 使用双转义符 \\\\ 即可<br>
     * 例：<br>
     * 通常使用：format("this is {} for {}", "a", "b") -> this is a for b<br>
     * 转义{}： format("this is \\{} for {}", "a", "b") -> this is \{} for a<br>
     * 转义\： format("this is \\\\{} for {}", "a", "b") -> this is \a for b<br>
     * 
     * @param strPattern 字符串模板
     * @param argArray 参数列表
     * @return 结果
     */
    public static String format(final String strPattern, final Object... argArray)
    {
        if (StringUtils.isEmpty(strPattern) || StringUtils.isEmpty(argArray))
        {
            return strPattern;
        }
        final int strPatternLength = strPattern.length();

        // 初始化定义好的长度以获得更好的性能
        StringBuilder sbuf = new StringBuilder(strPatternLength + 50);

        int handledPosition = 0;
        int delimIndex;// 占位符所在位置
        for (int argIndex = 0; argIndex < argArray.length; argIndex++)
        {
            delimIndex = strPattern.indexOf(EMPTY_JSON, handledPosition);
            if (delimIndex == -1)
            {
                if (handledPosition == 0)
                {
                    return strPattern;
                }
                else
                { // 字符串模板剩余部分不再包含占位符，加入剩余部分后返回结果
                    sbuf.append(strPattern, handledPosition, strPatternLength);
                    return sbuf.toString();
                }
            }
            else
            {
                if (delimIndex > 0 && strPattern.charAt(delimIndex - 1) == C_BACKSLASH)
                {
                    if (delimIndex > 1 && strPattern.charAt(delimIndex - 2) == C_BACKSLASH)
                    {
                        // 转义符之前还有一个转义符，占位符依旧有效
                        sbuf.append(strPattern, handledPosition, delimIndex - 1);
                        sbuf.append(Convert.toStr(argArray[argIndex]));
                        handledPosition = delimIndex + 2;
                    }
                    else
                    {
                        // 占位符被转义
                        argIndex--;
                        sbuf.append(strPattern, handledPosition, delimIndex - 1);
                        sbuf.append(C_DELIM_START);
                        handledPosition = delimIndex + 1;
                    }
                }
                else
                {
                    // 正常占位符
                    sbuf.append(strPattern, handledPosition, delimIndex);
                    sbuf.append(Convert.toStr(argArray[argIndex]));
                    handledPosition = delimIndex + 2;
                }
            }
        }
        // append the characters following the last {} pair.
        // 加入最后一个占位符后所有的字符
        sbuf.append(strPattern, handledPosition, strPattern.length());

        return sbuf.toString();
    }

    /**
     * 把参数argList替换到strPattern中的{*}中，中间用delimiter分隔。如果有多个，则多个都替换。
     * @param strPattern
     * @param delimiter: 分隔符
     * @param strCollection
     * @return
     */
    public static String formatWithCollection(final String strPattern, String delimiter, String prefix, Collection<String> strCollection){
        StringBuffer sb = new StringBuffer();
        int i = 0;
        for(String stri: strCollection){
            if(i++ > 0) {
                sb.append(delimiter);
            }
            sb.append(prefix + stri);
        }
        return strPattern.replaceAll("\\{\\*\\}",sb.toString());
    }

    /**
     * 格式化数值。
     * 1、“0”——表示一位数值;如没有，显示0。如“0000.0000”，整数位>4，按实际输出，<4前面补0，凑足4位。
     * 2、“#”——表示任何位数的整数。如没有，则不显示。在小数点模式后使用，只表示一位小数;四舍五入。如：
     *    # 无小数，小数部分四舍五入。
     *    .# 整数部分不变，一位小数，四舍五入。
     *    .## 整数部分不变，二位小数，四舍五入。
     * 3、“.”——表示小数点模式。
     * 4、“，”与模式“0”一起使用，表示逗号。
     * @param value
     * @return
     */
    public static String formatDecimal(Object value, String pattern){
        DecimalFormat nf = new DecimalFormat(pattern);
        return nf.format(value);
    }

    public static String formatInteger(Object value, int width){
        String pattern = "";
        for (int i = 0; i < width; i++) {
            pattern += "0";
        }
        return formatDecimal(value, pattern);
    }

    public static void main(String[] args){
        String toCrawlDate = "2020-01-30";
        Date parseDate = DateUtils.parseDate(toCrawlDate);
        String toCrawlDateStr = DateUtils.parseDateToStr("M/d/-yyyy", parseDate).replaceAll("/","%2F");
        Long hiddenDateParam = parseDate.getTime() + 28800000;
        String paramStr = StrFormatter.format("aaa{}bbbbbb{}", hiddenDateParam, toCrawlDateStr);
        System.out.println(paramStr);
    }
}
