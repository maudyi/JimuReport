package com.laker.admin.framework.utils;

import org.apache.commons.codec.binary.Base64;
import org.springframework.util.AntPathMatcher;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * 字符串工具类
 * 
 * @author ruoyi
 */
public class StringUtils extends org.apache.commons.lang3.StringUtils
{
    /** 空字符串 */
    private static final String NULLSTR = "";

    /** 下划线 */
    private static final char SEPARATOR = '_';

    /**
     * 获取参数不为空值
     * 
     * @param value defaultValue 要判断的value
     * @return value 返回值
     */
    public static <T> T nvl(T value, T defaultValue)
    {
        return value != null ? value : defaultValue;
    }

    /**
     * * 判断一个Collection是否为空， 包含List，Set，Queue
     * 
     * @param coll 要判断的Collection
     * @return true：为空 false：非空
     */
    public static boolean isEmpty(Collection<?> coll)
    {
        return isNull(coll) || coll.isEmpty();
    }

    /**
     * * 判断一个Collection是否非空，包含List，Set，Queue
     * 
     * @param coll 要判断的Collection
     * @return true：非空 false：空
     */
    public static boolean isNotEmpty(Collection<?> coll)
    {
        return !isEmpty(coll);
    }

    /**
     * * 判断一个对象数组是否为空
     * 
     * @param objects 要判断的对象数组
     ** @return true：为空 false：非空
     */
    public static boolean isEmpty(Object[] objects)
    {
        return isNull(objects) || (objects.length == 0);
    }

    /**
     * * 判断一个对象数组是否非空
     * 
     * @param objects 要判断的对象数组
     * @return true：非空 false：空
     */
    public static boolean isNotEmpty(Object[] objects)
    {
        return !isEmpty(objects);
    }

    /**
     * * 判断一个Map是否为空
     * 
     * @param map 要判断的Map
     * @return true：为空 false：非空
     */
    public static boolean isEmpty(Map<?, ?> map)
    {
        return isNull(map) || map.isEmpty();
    }

    /**
     * * 判断一个Map是否为空
     * 
     * @param map 要判断的Map
     * @return true：非空 false：空
     */
    public static boolean isNotEmpty(Map<?, ?> map)
    {
        return !isEmpty(map);
    }

    /**
     * * 判断一个字符串是否为空串
     * 
     * @param str String
     * @return true：为空 false：非空
     */
    public static boolean isEmpty(String str)
    {
        return isNull(str) || NULLSTR.equals(str.trim());
    }

    /**
     * 判断一个明知道是String的obj是否为空
     * @param str
     * @return
     */
    public static boolean isEmpty(Object str)
    {
        if(str instanceof String) {
            return isNull(str) || NULLSTR.equals(((String)str).trim());
        } else {
            return isNull(str) || NULLSTR.equals(str.toString().trim());
        }
    }

    /**
     * * 判断一个字符串是否为非空串
     * 
     * @param str String
     * @return true：非空串 false：空串
     */
    public static boolean isNotEmpty(String str)
    {
        return !isEmpty(str);
    }

    /**
     * 判断明知道是String对象的obj是否非空
     * @param str
     * @return
     */
    public static boolean isNotEmpty(Object str)
    {
        // 2020-8-14 maudyi，先判断null
        if(str == null){
            return false;
        }

        if(str instanceof String) {
            return !isEmpty((String)str);
        } else {
            return !isEmpty(str.toString());
        }
    }

    /**
     * * 判断一个对象是否为空
     * 
     * @param object Object
     * @return true：为空 false：非空
     */
    public static boolean isNull(Object object)
    {
        return object == null;
    }

    /**
     * * 判断一个对象是否非空
     * 
     * @param object Object
     * @return true：非空 false：空
     */
    public static boolean isNotNull(Object object)
    {
        return !isNull(object);
    }

    /**
     * * 判断一个对象是否是数组类型（Java基本型别的数组）
     * 
     * @param object 对象
     * @return true：是数组 false：不是数组
     */
    public static boolean isArray(Object object)
    {
        return isNotNull(object) && object.getClass().isArray();
    }

    /**
     * 去空格
     */
    public static String trim(String str)
    {
        return (str == null ? "" : str.trim());
    }

    /**
     * 截取字符串
     * 
     * @param str 字符串
     * @param start 开始
     * @return 结果
     */
    public static String substring(final String str, int start)
    {
        if (str == null)
        {
            return NULLSTR;
        }

        if (start < 0)
        {
            start = str.length() + start;
        }

        if (start < 0)
        {
            start = 0;
        }
        if (start > str.length())
        {
            return NULLSTR;
        }

        return str.substring(start);
    }

    /**
     * 截取字符串
     * 
     * @param str 字符串
     * @param start 开始
     * @param end 结束
     * @return 结果
     */
    public static String substring(final String str, int start, int end)
    {
        if (str == null)
        {
            return NULLSTR;
        }

        if (end < 0)
        {
            end = str.length() + end;
        }
        if (start < 0)
        {
            start = str.length() + start;
        }

        if (end > str.length())
        {
            end = str.length();
        }

        if (start > end)
        {
            return NULLSTR;
        }

        if (start < 0)
        {
            start = 0;
        }
        if (end < 0)
        {
            end = 0;
        }

        return str.substring(start, end);
    }

    /**
     * 格式化文本, {} 表示占位符<br>
     * 此方法只是简单将占位符 {} 按照顺序替换为参数<br>
     * 如果想输出 {} 使用 \\转义 { 即可，如果想输出 {} 之前的 \ 使用双转义符 \\\\ 即可<br>
     * 例：<br>
     * 通常使用：format("this is {} for {}", "a", "b") -> this is a for b<br>
     * 转义{}： format("this is \\{} for {}", "a", "b") -> this is \{} for a<br>
     * 转义\： format("this is \\\\{} for {}", "a", "b") -> this is \a for b<br>
     * 
     * @param template 文本模板，被替换的部分用 {} 表示
     * @param params 参数值
     * @return 格式化后的文本
     */
    public static String format(String template, Object... params)
    {
        if (isEmpty(params) || isEmpty(template))
        {
            return template;
        }
        return StrFormatter.format(template, params);
    }

    /**
     * 是否为http(s)://开头
     * 
     * @param link 链接
     * @return 结果
     */
    public static boolean ishttp(String link)
    {
        return StringUtils.startsWithAny(link, "http", "https");
    }



    /**
     * 查找指定字符串是否包含指定字符串列表中的任意一个字符串同时串忽略大小写
     *
     * @param cs 指定字符串
     * @param searchCharSequences 需要检查的字符串数组
     * @return 是否包含任意一个字符串
     */
    public static boolean containsAnyIgnoreCase(CharSequence cs, CharSequence... searchCharSequences)
    {
        if (isEmpty(cs) || isEmpty(searchCharSequences))
        {
            return false;
        }
        for (CharSequence testStr : searchCharSequences)
        {
            if (containsIgnoreCase(cs, testStr))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * 驼峰转下划线命名
     */
    public static String toUnderScoreCase(String str)
    {
        if (str == null)
        {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        // 前置字符是否大写
        boolean preCharIsUpperCase = true;
        // 当前字符是否大写
        boolean curreCharIsUpperCase = true;
        // 下一字符是否大写
        boolean nexteCharIsUpperCase = true;
        for (int i = 0; i < str.length(); i++)
        {
            char c = str.charAt(i);
            if (i > 0)
            {
                preCharIsUpperCase = Character.isUpperCase(str.charAt(i - 1));
            }
            else
            {
                preCharIsUpperCase = false;
            }

            curreCharIsUpperCase = Character.isUpperCase(c);

            if (i < (str.length() - 1))
            {
                nexteCharIsUpperCase = Character.isUpperCase(str.charAt(i + 1));
            }

            if (preCharIsUpperCase && curreCharIsUpperCase && !nexteCharIsUpperCase)
            {
                sb.append(SEPARATOR);
            }
            else if ((i != 0 && !preCharIsUpperCase) && curreCharIsUpperCase)
            {
                sb.append(SEPARATOR);
            }
            sb.append(Character.toLowerCase(c));
        }

        return sb.toString();
    }

    /**
     * 是否包含字符串
     * 
     * @param str 验证字符串
     * @param strs 字符串组
     * @return 包含返回true
     */
    public static boolean inStringIgnoreCase(String str, String... strs)
    {
        if (str != null && strs != null)
        {
            for (String s : strs)
            {
                if (str.equalsIgnoreCase(trim(s)))
                {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 删除最后一个字符串
     *
     * @param str 输入字符串
     * @param spit 以什么类型结尾的
     * @return 截取后的字符串
     */
    public static String lastStringDel(String str, String spit)
    {
        if (!StringUtils.isEmpty(str) && str.endsWith(spit))
        {
            return str.subSequence(0, str.length() - 1).toString();
        }
        return str;
    }

    /**
     * 将下划线大写方式命名的字符串转换为驼峰式。如果转换前的下划线大写方式命名的字符串为空，则返回空字符串。 例如：HELLO_WORLD->HelloWorld
     * 
     * @param name 转换前的下划线大写方式命名的字符串
     * @return 转换后的驼峰式命名的字符串
     */
    public static String convertToCamelCase(String name)
    {
        StringBuilder result = new StringBuilder();
        // 快速检查
        if (name == null || name.isEmpty())
        {
            // 没必要转换
            return "";
        }
        else if (!name.contains("_"))
        {
            // 不含下划线，仅将首字母大写
            return name.substring(0, 1).toUpperCase() + name.substring(1);
        }
        // 用下划线将原始字符串分割
        String[] camels = name.split("_");
        for (String camel : camels)
        {
            // 跳过原始字符串中开头、结尾的下换线或双重下划线
            if (camel.isEmpty())
            {
                continue;
            }
            // 首字母大写
            result.append(camel.substring(0, 1).toUpperCase());
            result.append(camel.substring(1).toLowerCase());
        }
        return result.toString();
    }

    /**
     * 驼峰式命名法
     * 例如：user_name->userName
     */
    public static String toCamelCase(String s)
    {
        if (s == null)
        {
            return null;
        }
        if (s.indexOf(SEPARATOR) == -1)
        {
            return s;
        }
        s = s.toLowerCase();
        StringBuilder sb = new StringBuilder(s.length());
        boolean upperCase = false;
        for (int i = 0; i < s.length(); i++)
        {
            char c = s.charAt(i);

            if (c == SEPARATOR)
            {
                upperCase = true;
            }
            else if (upperCase)
            {
                sb.append(Character.toUpperCase(c));
                upperCase = false;
            }
            else
            {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    /**
     * 查找指定字符串是否匹配指定字符串列表中的任意一个字符串
     * 
     * @param str 指定字符串
     * @param strs 需要检查的字符串数组
     * @return 是否匹配
     */
    public static boolean matches(String str, List<String> strs)
    {
        if (isEmpty(str) || isEmpty(strs))
        {
            return false;
        }
        for (String pattern : strs)
        {
            if (isMatch(pattern, str))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断url是否与规则配置: 
     * ? 表示单个字符; 
     * * 表示一层路径内的任意字符串，不可跨层级; 
     * ** 表示任意层路径;
     * 
     * @param pattern 匹配规则
     * @param url 需要匹配的url
     * @return
     */
    public static boolean isMatch(String pattern, String url)
    {
        AntPathMatcher matcher = new AntPathMatcher();
        return matcher.match(pattern, url);
    }

    @SuppressWarnings("unchecked")
    public static <T> T cast(Object obj)
    {
        return (T) obj;
    }

    /**
     * 数字左边补齐0，使之达到指定长度。注意，如果数字转换为字符串后，长度大于size，则只保留 最后size个字符。
     * 
     * @param num 数字对象
     * @param size 字符串指定长度
     * @return 返回数字的字符串格式，该字符串为指定长度。
     */
    public static final String padl(final Number num, final int size)
    {
        return padl(num.toString(), size, '0');
    }

    /**
     * 字符串左补齐。如果原始字符串s长度大于size，则只保留最后size个字符。
     * 
     * @param s 原始字符串
     * @param size 字符串指定长度
     * @param c 用于补齐的字符
     * @return 返回指定长度的字符串，由原字符串左补齐或截取得到。
     */
    public static final String padl(final String s, final int size, final char c)
    {
        final StringBuilder sb = new StringBuilder(size);
        if (s != null)
        {
            final int len = s.length();
            if (s.length() <= size)
            {
                for (int i = size - len; i > 0; i--)
                {
                    sb.append(c);
                }
                sb.append(s);
            }
            else
            {
                return s.substring(len - size, len);
            }
        }
        else
        {
            for (int i = size; i > 0; i--)
            {
                sb.append(c);
            }
        }
        return sb.toString();
    }
	
	
    /**
	 * maudyi added 首字母转小写: Pkid -> pkid
	 */
    public static String toLowerCaseFirstChar(String s){
        if(Character.isLowerCase(s.charAt(0))) {
            return s;
        }
        else {
            return (new StringBuilder()).append(Character.toLowerCase(s.charAt(0))).append(s.substring(1)).toString();
        }
    }


    /**
	 *  maudyi added 首字母转大写: pkid -> Pkid
	 */
    public static String toUpperCaseFirstChar(String s){
        if(Character.isUpperCase(s.charAt(0))) {
            return s;
        }
        else {
            return (new StringBuilder()).append(Character.toUpperCase(s.charAt(0))).append(s.substring(1)).toString();
        }
    }


    /**
     *  maudyi added 获取实质信息之前的空格个数。
     * @param content
     * @return
     */
    public static int getLengthOfIndent(String content){
        return getLengthOfIndent(content, " ".charAt(0));
    }

    /**
     *  maudyi added 获取实质信息之前的空白个数。可以指定是什么空白
     * @param content
     * @return
     */
    public static int getLengthOfIndent(String content, char c) {
        if(content == null || content.length() == 0){
            return 0;
        }
        int position = 0;
        int strLen = content.length();
        while (position < strLen && content.charAt(position) == c){
            position++;
        }
        return position;
    }

    /**
     *  maudyi added 使用gzip压缩字符串
     * @param str 要压缩的字符串
     * @return
     */
    public static String gzip(String str) {
        if (str == null || str.length() == 0) {
            return str;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        GZIPOutputStream gzip = null;
        try {
            gzip = new GZIPOutputStream(out);
            gzip.write(str.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (gzip != null) {
                try {
                    gzip.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return Base64.encodeBase64String(out.toByteArray());
    }

    /**
     *  maudyi added 使用gzip解压缩
     * @param compressedStr 压缩字符串
     * @return
     */
    public static String unGzip(String compressedStr) {
        if (compressedStr == null) {
            return null;
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayInputStream in = null;
        GZIPInputStream ginzip = null;
        byte[] compressed = null;
        String decompressed = null;
        try {
            compressed = Base64.decodeBase64(compressedStr);
            in = new ByteArrayInputStream(compressed);
            ginzip = new GZIPInputStream(in);
            byte[] buffer = new byte[1024];
            int offset = -1;
            while ((offset = ginzip.read(buffer)) != -1) {
                out.write(buffer, 0, offset);
            }
            decompressed = out.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (ginzip != null) {
                try {
                    ginzip.close();
                } catch (IOException e) {
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                }
            }
        }
        return decompressed;
    }

    /**
     * 从字符串中抽取Integer对象
     * @param source 原始字符串
     * @param len 抽取几位数以内的Integer值
     * @param returnFirst 为true时，匹配到第一个即返回，否则返回最后一个匹配项
     * @return
     */
    public static Integer[] extractIntFromStr(String source, int len, boolean returnFirst){
        List<Integer> tempList = new ArrayList<>();
        String[] numberInkeys = extractNumberStr(source, "(\\d{1," + len + "})", returnFirst);
        for (int i = 0; i < numberInkeys.length; i++) {
            if (numberInkeys[i].length() == 0) {
                return null;
            } else {
                tempList.add(Integer.valueOf(numberInkeys[i]));
            }
        }
        return tempList.toArray(new Integer[0]);
    }

    /**
     * 从字符串中抽取Long对象
     * @param source 原始字符串
     * @param len 抽取几位数以内的Long值
     * @param returnFirst 为true时，匹配到第一个即返回，否则返回最后一个匹配项
     * @return
     */
    public static Long[] extractLongFromStr(String source, int len, boolean returnFirst){
        List<Long> tempList = new ArrayList<>();
        String[] numberInkeys = extractNumberStr(source, "(\\d{1," + len + "})", returnFirst);
        for (int i = 0; i < numberInkeys.length; i++) {
            if(numberInkeys[i].length() == 0){
                return null;
            } else {
                tempList.add(Long.valueOf(numberInkeys[i]));
            }
        }
        return tempList.toArray(new Long[0]);
    }

    public static String[] extractNumberStr(String source, String patternString, boolean returnFirst) {
        List<String> tempList = new ArrayList<>();
        Pattern patternInt = Pattern.compile(patternString);
        Matcher matcherKey = patternInt.matcher(source);
        String numberInkey = "";
        while (matcherKey.find()) {
            numberInkey = matcherKey.group(0);
            tempList.add(numberInkey);
            if(returnFirst){
                break;
            }
        }
        String[] array = tempList.toArray(new String[0]);
        return array;
    }

    /**
     * 从字符串中抽取Double对象
     * @param source 原始字符串
     * @param returnFirst 为true时，匹配到第一个即返回，否则返回最后一个匹配项
     * @return
     */
    public static Double[] extractDoubleFromStr(String source, boolean returnFirst){
        List<Double> tempList = new ArrayList<>();
        String[] numberInkeys = extractNumberStr(source, "(\\d+(\\.\\d+)?)", returnFirst);
        for (int i = 0; i < numberInkeys.length; i++) {
            if (numberInkeys[i].length() == 0) {
                return null;
            } else {
                tempList.add(Double.valueOf(numberInkeys[i]));
            }
        }
        return tempList.toArray(new Double[0]);
    }

    /**
     * 从字符串中抽取yyyy-MM-dd格式的日期
     * @param source 原始字符串
     * @param returnFirst 为true时，匹配到第一个即返回，否则返回最后一个匹配项
     * @return
     */
    public static Date[] extractDateFromStr(String source, boolean returnFirst){
        List<Date> tempList = new ArrayList<>();
        String[] dateKeys = extractNumberStr(source, "(\\d{4}-\\d{1,2}-\\d{1,2})", returnFirst);
        for (int i = 0; i < dateKeys.length; i++) {
            if (dateKeys[i].length() == 0) {
                return null;
            } else {
                tempList.add(DateUtils.parseDate(dateKeys[i]));
            }
        }
        return tempList.toArray(new Date[0]);
    }

    /**
     * 模板解析
     *
     * @param template   模板字符串 如：My name is ${name}, i am ${age} years old! ${sayHai}
     * @param properties map接口的集合，如： {name: "azi", age: 18, sex: "man"}
     * @return: 解析后的字符串，如： My name is azi, i am 18 years old!
     * @author: 若非
     * @date: 2021/8/19 3:03
     */
    public static String parseTemplate(String template, Map properties) {
        if (template == null || template.isEmpty() || properties == null) {
            return template;
        }
        String r = "\\$\\{([^\\}]+)\\}";
        Pattern pattern = Pattern.compile(r);
        Matcher matcher = pattern.matcher(template);
        while (matcher.find()) {
            String group = matcher.group();
            Object o = properties.get(group.replaceAll(r, "$1"));
            if (o != null) {
                template = template.replace(group, String.valueOf(o));
            } else {
                template = template.replace(group, "");
            }
        }
        return template;
    }
    
    /**
     * 编辑距离算法
     * @param str1
     * @param str2
     */
    public static float wordsSimilarity(String str1, String str2) {
        //计算两个字符串的长度。
        int len1 = str1.length();
        int len2 = str2.length();
        //建立上面说的数组，比字符长度大一个空间
        int[][] dif = new int[len1 + 1][len2 + 1];
        //赋初值，步骤B。
        for (int a = 0; a <= len1; a++) {
            dif[a][0] = a;
        }
        for (int a = 0; a <= len2; a++) {
            dif[0][a] = a;
        }
        //计算两个字符是否一样，计算左上的值
        int temp;
        for (int i = 1; i <= len1; i++) {
            for (int j = 1; j <= len2; j++) {
                if (str1.charAt(i - 1) == str2.charAt(j - 1)) {
                    temp = 0;
                } else {
                    temp = 1;
                }
                //取三个值中最小的
                dif[i][j] = min(dif[i - 1][j - 1] + temp, dif[i][j - 1] + 1,
                        dif[i - 1][j] + 1);
            }
        }
        //计算相似度
        float similarity =1 - (float) dif[len1][len2] / Math.max(str1.length(), str2.length());
        return similarity;
    }
    
    
    /**
     * 获取最匹配的项
     * @param dataMap
     * @param input
     * @return 最匹配项的key，如果都低于阈值，就返回null
     */
    public static Object wordsSimilarityMax(Map<Object, String> dataMap, String input, Float factor){
        if(factor == null){
            factor = 0.8F;
        }
        Object matchedKey = null;
        float maxSimilar = 0;
        for (Object key : dataMap.keySet()) {
            String value = dataMap.get(key);
            float similarity = wordsSimilarity(value, input);
            if(similarity > maxSimilar){
                maxSimilar = similarity;
                matchedKey = key;
            }
        }
        if(maxSimilar > factor){
            return matchedKey;
        } else {
            return null;
        }
    }
    
    /**
     * 只要sourceStr中包含arrayToCheck里面任意一个元素，就返回1，全都不包含则返回-1
     * @param sourceStr
     * @param arrayToCheck
     * @return
     */
    public static int arrayContains(String sourceStr, String[] arrayToCheck) {
        for (int i = 0; i < arrayToCheck.length; i++) {
            if(sourceStr.indexOf(arrayToCheck[i]) != -1){
                return 1;
            }
        }
        return -1;
    }

    //得到最小值
    private static int min(int... is) {
        int min = Integer.MAX_VALUE;
        for (int i : is) {
            if (min > i) {
                min = i;
            }
        }
        return min;
    }



    private static void showLang3StringUtilsUsage() {
        //null 和 ""操作~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //判断是否Null 或者 ""
        System.out.println(StringUtils.isEmpty((Object[]) null));
        System.out.println(StringUtils.isNotEmpty((Object[]) null));
        //判断是否null 或者 "" 去空格~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        System.out.println(StringUtils.isBlank("  "));
        System.out.println(StringUtils.isNotBlank(null));
        //去空格.Null返回null~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        System.out.println(StringUtils.trim(null));
        //去空格，将Null和"" 转换为Null
        System.out.println(StringUtils.trimToNull(""));
        //去空格，将NULL 和 "" 转换为""
        System.out.println(StringUtils.trimToEmpty(null));
        //可能是对特殊空格符号去除？？
        System.out.println(StringUtils.strip("大家好  啊  \t"));
        //同上，将""和null转换为Null
        System.out.println(StringUtils.stripToNull(" \t"));
        //同上，将""和null转换为""
        System.out.println(StringUtils.stripToEmpty(null));
        //将""或者Null 转换为 ""
        System.out.println(StringUtils.defaultString(null));
        //仅当字符串为Null时 转换为指定的字符串(二参数)
        System.out.println(StringUtils.defaultString("", "df"));
        //当字符串为null或者""时，转换为指定的字符串(二参数)
        System.out.println(StringUtils.defaultIfEmpty(null, "sos"));
        //去空格.去字符~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //如果第二个参数为null去空格(否则去掉字符串2边一样的字符，到不一样为止)
        System.out.println(StringUtils.strip("fsfsdf", "f"));
        //如果第二个参数为null只去前面空格(否则去掉字符串前面一样的字符，到不一样为止)
        System.out.println(StringUtils.stripStart("ddsuuu ", "d"));
        //如果第二个参数为null只去后面空格，(否则去掉字符串后面一样的字符，到不一样为止)
        System.out.println(StringUtils.stripEnd("dabads", "das"));
        //对数组没个字符串进行去空格。
        System.out.println(StringUtils.stripAll(new String[]{" 中华 ", "民 国 ", "共和 "}));
        //如果第二个参数为null.对数组每个字符串进行去空格。(否则去掉数组每个元素开始和结尾一样的字符)
        System.out.println(StringUtils.stripAll(new String[]{" 中华 ", "民 国", "国共和国"}, "国"));
        //查找,判断~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //判断2个字符串是否相等相等,Null也相等
        System.out.println(StringUtils.equals(null, null));
        //不区分大小写比较
        System.out.println(StringUtils.equalsIgnoreCase("abc", "ABc"));
        //查找，不知道怎么弄这么多查找，很多不知道区别在哪？费劲~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //普通查找字符，如果一参数为null或者""返回-1
        System.out.println(StringUtils.indexOf(null, "a"));
        //从指定位置(三参数)开始查找，本例从第2个字符开始查找k字符
        System.out.println(StringUtils.indexOf("akfekcd中华", "k", 2));
        //未发现不同之处
        System.out.println(StringUtils.ordinalIndexOf("akfekcd中华", "k", 2));
        //查找,不区分大小写
        System.out.println(StringUtils.indexOfIgnoreCase("adfs", "D"));
        //从指定位置(三参数)开始查找,不区分大小写
        System.out.println(StringUtils.indexOfIgnoreCase("adfs", "a", 3));
        //从后往前查找
        System.out.println(StringUtils.lastIndexOf("adfas", "a"));
        //未理解,此结果为2
        System.out.println(StringUtils.lastIndexOf("d饿abasdafs我", "a", 3));
        //未解,此结果为-1
        System.out.println(StringUtils.lastOrdinalIndexOf("yksdfdht", "f", 2));
        //从后往前查，不区分大小写
        System.out.println(StringUtils.lastIndexOfIgnoreCase("sdffet", "E"));
        //未解,此结果为1
        System.out.println(StringUtils.lastIndexOfIgnoreCase("efefrfs看", "F", 2));
        //检查是否查到，返回boolean,null返回假
        System.out.println(StringUtils.contains("sdf", "dg"));
        //检查是否查到，返回boolean,null返回假,不区分大小写
        System.out.println(StringUtils.containsIgnoreCase("sdf", "D"));
        //检查是否有含有空格,返回boolean
        System.out.println(StringUtils.containsWhitespace(" d"));
        //查询字符串跟数组任一元素相同的第一次相同的位置
        System.out.println(StringUtils.indexOfAny("absfekf", new String[]{"f", "b"}));
        //查询字符串中指定字符串(参数二)出现的次数
        System.out.println(StringUtils.indexOfAny("afefes", "e"));
        //查找字符串中是否有字符数组中相同的字符，返回boolean
        System.out.println(StringUtils.containsAny("asfsd", new char[]{'k', 'e', 's'}));
        //未理解与lastIndexOf不同之处。是否查到，返回boolean
        System.out.println(StringUtils.containsAny("啡f咖啡", "咖"));
        //未解
        System.out.println(StringUtils.indexOfAnyBut("seefaff", "af"));
        //判断字符串中所有字符，都是出自参数二中。
        System.out.println(StringUtils.containsOnly("中华华", "华"));
        //判断字符串中所有字符，都是出自参数二的数组中。
        System.out.println(StringUtils.containsOnly("中华中", new char[]{'中', '华'}));
        //判断字符串中所有字符，都不在参数二中。
        System.out.println(StringUtils.containsNone("中华华", "国"));
        //判断字符串中所有字符，都不在参数二的数组中。
        System.out.println(StringUtils.containsNone("中华中", new char[]{'中', '达'}));
        //从后往前查找字符串中与字符数组中相同的元素第一次出现的位置。本例为4
        System.out.println(StringUtils.lastIndexOfAny("中国人民共和国", new String[]{"国人", "共和"}));
        //未发现与indexOfAny不同之处  查询字符串中指定字符串(参数二)出现的次数
        System.out.println(StringUtils.countMatches("中国人民共和中国", "中国"));
        //检查是否CharSequence的只包含Unicode的字母。空将返回false。一个空的CharSequence（长（）= 0）将返回true
        System.out.println(StringUtils.isAlpha("这是干什么的2"));
        //检查是否只包含Unicode的CharSequence的字母和空格（''）。空将返回一个空的CharSequence假（长（）= 0）将返回true。
        System.out.println(StringUtils.isAlphaSpace("NBA直播 "));
        //检查是否只包含Unicode的CharSequence的字母或数字。空将返回false。一个空的CharSequence（长（）= 0）将返回true。
        System.out.println(StringUtils.isAlphanumeric("NBA直播"));
        //如果检查的Unicode CharSequence的只包含字母，数字或空格（''）。空将返回false。一个空的CharSequence（长（）= 0）将返回true。
        System.out.println(StringUtils.isAlphanumericSpace("NBA直播"));
        //检查是否只包含ASCII可CharSequence的字符。空将返回false。一个空的CharSequence（长（）= 0）将返回true。
        System.out.println(StringUtils.isAsciiPrintable("NBA直播"));
        //检查是否只包含数值。
        System.out.println(StringUtils.isNumeric("NBA直播"));
        //检查是否只包含数值或者空格
        System.out.println(StringUtils.isNumericSpace("33 545"));
        //检查是否只是空格或""。
        System.out.println(StringUtils.isWhitespace(" "));
        //检查是否全是英文小写。
        System.out.println(StringUtils.isAllLowerCase("kjk33"));
        //检查是否全是英文大写。
        System.out.println(StringUtils.isAllUpperCase("KJKJ"));
        //交集操作~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //去掉参数2字符串中在参数一中开头部分共有的部分，结果为:人民共和加油
        System.out.println(StringUtils.difference("中国加油", "中国人民共和加油"));
        //统计2个字符串开始部分共有的字符个数
        System.out.println(StringUtils.indexOfDifference("ww.taobao", "www.taobao.com"));
        //统计数组中各个元素的字符串开始都一样的字符个数
        System.out.println(StringUtils.indexOfDifference(new String[] {"中国加油", "中国共和", "中国人民"}));
        //取数组每个元素共同的部分字符串
        System.out.println(StringUtils.getCommonPrefix(new String[] {"中国加油", "中国共和", "中国人民"}));
        //统计参数一中每个字符与参数二中每个字符不同部分的字符个数
        System.out.println(StringUtils.getLevenshteinDistance("中国共和发国人民", "共和国"));
        //判断开始部分是否与二参数相同
        System.out.println(StringUtils.startsWith("中国共和国人民", "中国"));
        //判断开始部分是否与二参数相同。不区分大小写
        System.out.println(StringUtils.startsWithIgnoreCase("中国共和国人民", "中国"));
        //判断字符串开始部分是否与数组中的某一元素相同
        System.out.println(StringUtils.startsWithAny("abef", new String[]{"ge", "af", "ab"}));
        //判断结尾是否相同
        System.out.println(StringUtils.endsWith("abcdef", "def"));
        //判断结尾是否相同，不区分大小写
        System.out.println(StringUtils.endsWithIgnoreCase("abcdef", "Def"));
        //字符串截取~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //截取指定位置的字符，null返回null.""返回""
        System.out.println(StringUtils.substring("国民党", 2));
        //截取指定区间的字符
        System.out.println(StringUtils.substring("中国人民共和国", 2, 4));
        //从左截取指定长度的字符串
        System.out.println(StringUtils.left("说点什么好呢", 3));
        //从右截取指定长度的字符串
        System.out.println(StringUtils.right("说点什么好呢", 3));
        //从第几个开始截取，三参数表示截取的长度
        System.out.println(StringUtils.mid("说点什么好呢", 3, 2));
        //截取到等于第二个参数的字符串为止
        System.out.println(StringUtils.substringBefore("说点什么好呢", "好"));
        //从左往右查到相等的字符开始，保留后边的，不包含等于的字符。本例：什么好呢
        System.out.println(StringUtils.substringAfter("说点什么好呢", "点"));
        //这个也是截取到相等的字符，但是是从右往左.本例结果：说点什么好
        System.out.println(StringUtils.substringBeforeLast("说点什么好点呢", "点"));
        //这个截取同上是从右往左。但是保留右边的字符
        System.out.println(StringUtils.substringAfterLast("说点什么好点呢？", "点"));
        //截取查找到第一次的位置，和第二次的位置中间的字符。如果没找到第二个返回null。本例结果:2010世界杯在
        System.out.println(StringUtils.substringBetween("南非2010世界杯在南非，在南非", "南非"));
        //返回参数二和参数三中间的字符串，返回数组形式
        System.out.println(StringUtils.substringsBetween("[a][b][c]", "[", "]"));
        //分割~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //用空格分割成数组，null为null
        System.out.println(StringUtils.split("中华 人民  共和"));
        //以指定字符分割成数组
        System.out.println(StringUtils.split("中华 ,人民,共和", ","));
        //以指定字符分割成数组，第三个参数表示分隔成数组的长度，如果为0全体分割
        System.out.println(StringUtils.split("中华 ：人民：共和", "：", 2));
        //未发现不同的地方,指定字符分割成数组
        System.out.println(StringUtils.splitByWholeSeparator("ab-!-cd-!-ef", "-!-"));
        //未发现不同的地方,以指定字符分割成数组，第三个参数表示分隔成数组的长度
        System.out.println(StringUtils.splitByWholeSeparator("ab-!-cd-!-ef", "-!-", 2));
        //分割，但" "不会被忽略算一个元素,二参数为null默认为空格分隔
        System.out.println(StringUtils.splitByWholeSeparatorPreserveAllTokens(" ab   de fg ", null));
        //同上，分割," "不会被忽略算一个元素。第三个参数代表分割的数组长度。
        System.out.println(StringUtils.splitByWholeSeparatorPreserveAllTokens("ab   de fg", null, 3));
        //未发现不同地方,分割
        System.out.println(StringUtils.splitPreserveAllTokens(" ab   de fg "));
        //未发现不同地方,指定字符分割成数组
        System.out.println(StringUtils.splitPreserveAllTokens(" ab   de fg ", null));
        //未发现不同地方,以指定字符分割成数组，第三个参数表示分隔成数组的长度
        System.out.println(StringUtils.splitPreserveAllTokens(" ab   de fg ", null, 2));
        //以不同类型进行分隔
        System.out.println(StringUtils.splitByCharacterType("AEkjKr i39:。中文"));
        //未解
        System.out.println(StringUtils.splitByCharacterTypeCamelCase("ASFSRules234"));
        //拼接~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //也是拼接。未发现区别
        System.out.println(StringUtils.join(new ArrayList<String>()));
        //用连接符拼接，为发现区别
        System.out.println(StringUtils.join(new ArrayList<String>(), ":"));
        //拼接指定数组下标的开始(三参数)和结束(四参数,不包含)的中间这些元素，用连接符连接
        ArrayList<String> list = new ArrayList<>();
        list.add("first");
        list.add("second");
        list.add("third");
        System.out.println(StringUtils.join(list, ":", 1, 3));
        //用于集合连接字符串.用于集合
        System.out.println(StringUtils.join(new ArrayList<String>(), ":"));
        //移除，删除~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //删除所有空格符
        System.out.println(StringUtils.deleteWhitespace(" s 中 你 4j"));
        //移除开始部分的相同的字符
        System.out.println(StringUtils.removeStart("www.baidu.com", "www."));
        //移除开始部分的相同的字符,不区分大小写
        System.out.println(StringUtils.removeStartIgnoreCase("www.baidu.com", "WWW"));
        //移除后面相同的部分
        System.out.println(StringUtils.removeEnd("www.baidu.com", ".com"));
        //移除后面相同的部分，不区分大小写
        System.out.println(StringUtils.removeEndIgnoreCase("www.baidu.com", ".COM"));
        //移除所有相同的部分
        System.out.println(StringUtils.remove("www.baidu.com/baidu", "bai"));
        //移除结尾字符为"\n", "\r", 或者 "\r\n".
        System.out.println(StringUtils.chomp("abcrabc\r"));
        //也是移除，未解。去结尾相同字符
        System.out.println(StringUtils.chomp("baidu.com", "com"));
        //去掉末尾最后一个字符.如果是"\n", "\r", 或者 "\r\n"也去除
        System.out.println(StringUtils.chop("wwe.baidu"));
        //替换~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //替换指定的字符，只替换第一次出现的
        System.out.println(StringUtils.replaceOnce("www.baidu.com/baidu", "baidu", "hao123"));
        //替换所有出现过的字符
        System.out.println(StringUtils.replace("www.baidu.com/baidu", "baidu", "hao123"));
        //也是替换，最后一个参数表示替换几个
        System.out.println(StringUtils.replace("www.baidu.com/baidu", "baidu", "hao123", 1));
        //这个有意识，二三参数对应的数组，查找二参数数组一样的值，替换三参数对应数组的值。本例:baidu替换为taobao。com替换为net
        System.out.println(StringUtils.replaceEach("www.baidu.com/baidu", new String[]{"baidu", "com"}, new String[]{"taobao", "net"}));
        //同上，未发现不同
        System.out.println(StringUtils.replaceEachRepeatedly("www.baidu.com/baidu", new String[]{"baidu", "com"}, new String[]{"taobao", "net"}));
        //这个更好，不是数组对应，是字符串参数二和参数三对应替换.(二三参数不对应的话，自己看后果)
        System.out.println(StringUtils.replaceChars("www.baidu.com", "bdm", "qo"));
        //替换指定开始(参数三)和结束(参数四)中间的所有字符
        System.out.println(StringUtils.overlay("www.baidu.com", "hao123", 4, 9));
        //添加，增加~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //复制参数一的字符串，参数二为复制的次数
        System.out.println(StringUtils.repeat("ba", 3));
        //复制参数一的字符串，参数三为复制的次数。参数二为复制字符串中间的连接字符串
        System.out.println(StringUtils.repeat("ab", "ou", 3));
        //如何字符串长度小于参数二的值，末尾加空格补全。(小于字符串长度不处理返回)
        System.out.println(StringUtils.rightPad("海川", 4));
        //字符串长度小于二参数，末尾用参数三补上，多于的截取(截取补上的字符串)
        System.out.println(StringUtils.rightPad("海川", 4, "河流啊"));
        //同上在前面补全空格
        System.out.println(StringUtils.leftPad("海川", 4));
        //字符串长度小于二参数，前面用参数三补上，多于的截取(截取补上的字符串)
        System.out.println(StringUtils.leftPad("海川", 4, "大家好"));
        //字符串长度小于二参数。在两侧用空格平均补全（测试后面补空格优先）
        System.out.println(StringUtils.center("海川", 3));
        //字符串长度小于二参数。在两侧用三参数的字符串平均补全（测试后面补空格优先）
        System.out.println(StringUtils.center("海川", 5, "流"));
        //只显示指定数量(二参数)的字符,后面以三个点补充(参数一截取+三个点=二参数)
        System.out.println(StringUtils.abbreviate("中华人民共和国", 5));
        //2头加点这个有点乱。本例结果: ...ijklmno
        System.out.println(StringUtils.abbreviate("abcdefghijklmno", 12, 10));
        //保留指定长度，最后一个字符前加点.本例结果: ab.f
        System.out.println(StringUtils.abbreviateMiddle("abcdef", ".", 4));
        //转换,刷选~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //转换第一个字符为大写.如何第一个字符是大写原始返回
        System.out.println(StringUtils.capitalize("Ddf"));
        //转换第一个字符为大写.如何第一个字符是大写原始返回
        System.out.println(StringUtils.uncapitalize("DTf"));
        //反向转换，大写变小写，小写变大写
        System.out.println(StringUtils.swapCase("I am Jiang, Hello"));
        //将字符串倒序排列
        System.out.println(StringUtils.reverse("中国人民"));
        //根据特定字符(二参数)分隔进行反转
        System.out.println(StringUtils.reverseDelimited("中:国:人民", ':'));
    }
	
	
    public static void main(String[] args) {
    	// lang3的 StringUtils的各个方法的使用示例：
        // showLang3StringUtilsUsage();

        Date[] dates = extractDateFromStr("今天是2022-03-31，天气晴，明天2022-8-10，有雨", false);
        Arrays.stream(dates).forEach(e -> System.out.println(e));
    }
}