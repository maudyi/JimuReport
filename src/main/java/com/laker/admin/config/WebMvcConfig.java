package com.laker.admin.config;

import cn.dev33.satoken.interceptor.SaAnnotationInterceptor;
import com.laker.admin.framework.ext.interceptor.TraceAnnotationInterceptor;
import cn.dev33.satoken.interceptor.SaRouteInterceptor;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.stp.StpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;
import java.io.File;

@Configuration
@Slf4j
public class WebMvcConfig implements WebMvcConfigurer {

    // 2022-9-22，maudyi，for jimureport，放行，到JimuReportAuthInterceptor 去认证
    private static final String[] exclude_path = {"/admin/**",
            "/admin/login.html",
            "/error",
            "/swagger-resources/**",
            "/api/v1/login",
            "/captcha",
            "/thumbnail",
            "/jmreport/view/**",
            "/jmreport/shareView/**",
            "/jmreport/getQueryInfo",
            "/jmreport/addViewCount/**",
            "/jmreport/checkParam/**",
            "/jmreport/share/verification",
            "/jmreport/show",
            "/jmreport/userinfo",
            // 使用ip情况下，列表页url不能跟浏览器登录用户共享session，报401，所以只能放行了。
            "/jmreport/**"
    };

    private static final String[] trace_exclude_path = {"/admin/**",
            "/admin/login.html",
            "/error",
            "/swagger-resources/**",
            "/api/v1/login",
            "/captcha",
            "/thumbnail",
            "/jmreport/view/**",
            "/jmreport/shareView/**",
            "/jmreport/getQueryInfo",
            "/jmreport/addViewCount/**",
            "/jmreport/checkParam/**",
            "/jmreport/share/verification",
            "/jmreport/show",
            // 使用ip情况下，列表页url不能跟浏览器登录用户共享session，报401，所以只能放行了。
            "/jmreport/**"
    };
    
    @Resource
    LakerConfig lakerConfig;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowCredentials(true)
                .allowedMethods("*")
                .maxAge(3600);
    }

    /**
     * 注册sa-token的拦截器，打开注解式鉴权功能 (如果您不需要此功能，可以删除此类)
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册注解拦截器，并排除不需要注解鉴权的接口地址 (与登录拦截器无关)
        // 2022-9-22，maudyi，for jimureport，无参构造写法，代表只进行默认的登录校验功能
        registry.addInterceptor(new SaAnnotationInterceptor()).addPathPatterns("/**")
                .excludePathPatterns(exclude_path);
        // 2022-9-22，maudyi，for jimureport，注册 Sa-Token 的路由拦截器，自定义认证规则
        registry.addInterceptor(new SaRouteInterceptor((req, res, handler)->{
            // 根据路由划分模块，不同模块不同鉴权
            SaRouter.match("/user/**", () -> StpUtil.checkPermission("user"));
        })).addPathPatterns("/**");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        File web = new File("web");
        String path = lakerConfig.getOssFile().getPath();
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }
        log.info(file.getAbsolutePath());
        registry.addResourceHandler("/admin/**")
                .addResourceLocations("file:" + web.getAbsolutePath() + "/admin/");

        registry.addResourceHandler("/" + path + "/**")
                .addResourceLocations("file:" + file.getAbsolutePath() + "/");
    }

}
