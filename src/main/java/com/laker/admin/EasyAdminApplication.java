package com.laker.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

/**
 *
 * @author laker
 * 2022-9-22 maudyi for jimureport
 */
@SpringBootApplication
        (exclude={MongoAutoConfiguration.class}, scanBasePackages = {"com.jeecg.modules.jmreport","com.laker.admin"})
public class EasyAdminApplication {
    public static void main(String[] args) {
	    // 2022-9-22,maudyi,for jimureport. 运行JimuReportTokenService的genToken 方法生成token。目前是一个固定的token，将来再考虑变成从数据表中获取。
        ConfigurableApplicationContext application = SpringApplication.run(EasyAdminApplication.class, args);
        Environment env = application.getEnvironment();
        String port = env.getProperty("server.port");
        String path = env.containsProperty("server.servlet.context-path")?env.getProperty("server.servlet.context-path"):"";
        System.out.print("\n----------------------------------------------------------\n\t" +
                "Application JimuReport Demo is running! Access URL:\n\t" +
                "Local: \t\thttp://localhost:" + port + path + "/jmreport/list\n\t" +
                "----------------------------------------------------------");
    }
}
