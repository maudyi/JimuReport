package com.jeecg.modules.jmreport.interceptors;

import com.jeecg.modules.jmreport.config.JimuReportTokenService;
import com.laker.admin.framework.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.AsyncHandlerInterceptor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.MediaType;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 对积木报表的权限认证拦截器。
 * url：jmreport/view/*
 */
@Log4j2
@Component
public class JimuReportAuthInterceptor implements AsyncHandlerInterceptor {

    /**报表查看页面在easy-admin中是匿名访问，但从外部访问需要单独检查token*/
    public static final String[] JMREPORT_VIEW = {
            "/jmreport/view/",
            "/jmreport/show",
            "/jmreport/share/",
            "/jmreport/shareView/",
            "/jmreport/addViewCount/",
            "/jmreport/getQueryInfo/",
            "/jmreport/checkParam/",
            // 使用ip情况下，列表页url不能跟浏览器登录用户共享session，报401，所以只能在这里校验了。
            "/jmreport/list/",
    };

    @Autowired
    private JimuReportTokenService jimuReportTokenService;

    /**
     * 请求处理前调用
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if(StringUtils.arrayContains(request.getRequestURI(), JMREPORT_VIEW) != -1){
            String token = jimuReportTokenService.getToken();
            Boolean verifyToken = jimuReportTokenService.verifyToken(token);
            log.info("VIEW = {}, token={}, verifyToken={}",request.getRequestURI(), token, verifyToken);
            return verifyToken;
        } else {
            log.info("URL = {}, ignored.", request.getRequestURI());
            return true;
        }
    }

    /**
     * 请求处理完后渲染视图前调用
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable ModelAndView modelAndView) throws Exception {
    }

    /**
     * 渲染视图后调用
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
    }
}
