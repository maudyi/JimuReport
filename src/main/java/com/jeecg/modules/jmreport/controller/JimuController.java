package com.jeecg.modules.jmreport.controller;

import com.jeecg.modules.jmreport.config.JimuReportTokenService;
import com.laker.admin.framework.model.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 积木报表-设置默认首页跳转
 */
@Controller
public class JimuController {
    private Logger logger = LoggerFactory.getLogger(JimuController.class);

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("name", "jimureport");
        return "jmreport/list"; // 视图重定向 - 跳转
    }


    @PostMapping("/genToken")
    @ResponseBody
    public Response genToken(Model model) {
        String token = JimuReportTokenService.genToken();
        return Response.ok(token);
    }
}