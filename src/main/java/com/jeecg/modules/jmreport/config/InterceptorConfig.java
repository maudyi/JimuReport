package com.jeecg.modules.jmreport.config;

import com.jeecg.modules.jmreport.interceptors.JimuReportAuthInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.annotation.Resource;

/**
 * 加入URL拦截
 * @author tyg
 * @date 2020-12-31 14:59
 */
@Configuration
public class InterceptorConfig extends WebMvcConfigurerAdapter {
    @Bean
    public HandlerInterceptor getMyInterceptor(){
        return authHandlerInterceptor;
    }



    private static final String[] jimuReports = {"/**"};
//    private static final String[] excludePatterns = {"jmreport/list/**"};

    @Resource
    private JimuReportAuthInterceptor authHandlerInterceptor;

    /**
     * 加入URL拦截
     * @param registry
     * @author tyg
     * @date 2020-12-31 14:59
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authHandlerInterceptor).addPathPatterns(jimuReports)
//                .excludePathPatterns(excludePatterns)
        ;
        super.addInterceptors(registry);
    }

}
