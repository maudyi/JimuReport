package com.jeecg.modules.jmreport.config;

import cn.hutool.jwt.JWT;
import cn.hutool.jwt.signers.JWTSignerUtil;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.modules.jmreport.api.JmReportTokenServiceI;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * 自定义积木报表鉴权(如果不进行自定义，则所有请求不做权限控制)
 * 1.自定义获取登录token
 * 2.自定义获取登录用户
 */
@Component
public class JimuReportTokenService implements JmReportTokenServiceI {

    /**存储几个固定的用户信息*/
    private static Map<String, String> usersMap = new HashMap<>();

    public static final String PASSWORD = "Xinxianquan@15";

    static {
        usersMap.put("admin", PASSWORD);
    }


    @Override
    public String getUsername(String token) {
        Object name = JWT.of(token).getPayload("name");
        return name != null ? name.toString() : "anno";
    }

    @Override
    public Boolean verifyToken(String token) {
        if(token == null || token.length() == 0 || token.equalsIgnoreCase("null")){
            return false;
        }
        for (String key : usersMap.keySet()) {
            // 密钥
            byte[] pass = usersMap.get(key).getBytes();
            boolean verify = JWT.of(token).setKey(pass).verify();
            if(verify){
                return true;
            }
        }
        return false;
    }

    @Override
    public Map<String, Object> getUserInfo(String token) {
        Map<String, Object> map = new HashMap(5);
        JWT jwt = JWT.of(token);
        String username = jwt.getPayload("uid").toString();
        map.put(SYS_USER_CODE, username);
        //设置部门编码
        map.put(SYS_ORG_CODE, "Org1");
        // 将所有信息存放至map 解析sql/api会根据map的键值解析
        return map;
    }


    /**
     * 生成token，加在菜单末尾。
     */
    public static String genToken() {
        Map<String, Object> map = new HashMap<String, Object>() {
            private static final long serialVersionUID = 1L;
            {
                put("uid", "admin");
                put("expire_time", System.currentTimeMillis() + 1000 * 60 * 60 * 24 * 150);
            }
        };

        String token = cn.hutool.jwt.JWTUtil.createToken(map, PASSWORD.getBytes());
        return token;
    }

}